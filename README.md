docker-chromedriver
===================

Container image with chromedriver and headless chromium, based on Alpine linux.

Chromedriver is listening on port 4444.


Usage
-----

The image is available as: `registry.gitlab.com/denkmal/docker-chromedriver:latest`.
Check the container registry for more tags.

Development
-----------

Build the image:
```
podman build -t docker-chromedriver .
```

Run the image:
```
podman run --rm -p 4444:4444 docker-chromedriver
```

Release
-------

The image is built and pushed to the registry by Gitlab CI.
