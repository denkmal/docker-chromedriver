# From https://github.com/retreatguru/headless-chromedriver
FROM docker.io/library/alpine:3.9

# install chromium and chromedriver
RUN apk add --no-cache \
    dumb-init \
    chromium \
    chromium-chromedriver

## replace chromium binary with a script that has some default options
RUN mv /usr/lib/chromium/chrome /usr/lib/chromium/chrome-bin
COPY ./config/chrome /usr/lib/chromium/

EXPOSE 4444
EXPOSE 9222
ENTRYPOINT ["/usr/bin/dumb-init", "--"]
CMD ["chromedriver", "--port=4444", "--whitelisted-ips="]
